import React, {useState} from "react";
import CmsInput from "../CmsInput/CmsInput";
import CmsInputButton from "../CmsInputButton/CmsInputButton";
import { Clear } from "@material-ui/icons";
import baseUrl from "../../helpers/baseUrl";
import Loader from "react-js-loader"
import axios from "axios";
import toaster from "../../helpers/toaster";

import './categoryModal.scss'
function CategoryModal({open}) {
  const [category, SetCategory] = useState('')
  const [loading, setLoading] = useState(false)
  
  const handleClick = (e) => {
    const target = e.target.className
    if(target === 'category_modal') open(false)
    return
  }

  const handleChange = (e) => {
    SetCategory(e.target.value)
  }

  const CreateCat = async(e) => {
    e.preventDefault();
    setLoading(true)
    const url = baseUrl + "categories";
    const payload = { "categoryName": category };
    
    try {
      if(!category) throw new Error('Please enter a category')
      const {data} = await axios.post(url, payload)
      setLoading(false);
      const state = (data.status === 200) ? "success" : "error"
      toaster(state, data.message, 2000);
      SetCategory("")
      
    } catch (error) {
      const message = error.response ? error.response.data.message : error.message
      setLoading(false);
      toaster("error", message, 2000);
    }

  };

  return (
    <div className="category_modal" onClick={(e) => handleClick(e)}>
      <div className="category_modal__form">
        <Clear onClick={() => open(false)} />
        <div className="form-control">
          <label htmlFor="title"> Enter new Category below </label>
          <CmsInput
            id="title"
            placeholder="Health and wellness"
            value={category}
            handler={handleChange}
          />
        </div>
        <div className="saveBtn">
          <CmsInputButton
            label="Save"
            loader={
              loading ? (
                <Loader
                  type="spinner-cub"
                  bgColor={"#fff"}
                  title={""}
                  color={"#000000"}
                  size={20}
                />
              ) : (
                ""
              )
            }
            handler={CreateCat}
          />
        </div>
      </div>
    </div>
  );
}

export default CategoryModal;
