import React from 'react'
import { useState } from 'react';
import { usePaystackPayment } from 'react-paystack';
import { useStateValue } from '../helpers/stateProvider'
import CircularProgress from '@material-ui/core/CircularProgress';
import baseUrl from '../helpers/baseUrl';
import toaster from '../helpers/toaster';
import { ToastContainer } from 'react-toastify';

function Checkout({amount}) {
   const userEmail = sessionStorage.getItem("email");
      const [spinner, setSpinner] = useState(false)

    const [{ basket}, dispatch] = useStateValue();
     const [errorMessage, setErrorMessage] = useState('');
    const newData = basket.map(item=>{
        // console.log(item);
        let properties = {
          id: item.id,
          item_name: item.name,
          sku: item.sku,
          quantity: item.quantityValue,
          price: item.price,
          imgUrl: item.image
        };
        return properties;
    })
   
    const styles={
        padding:"20px",
        backgroundColor:"#141414",
        color:"#fff",
        width:"100%",
        border:'none',
        cursor:"pointer",
        fontSize:"1.1rem",
        fontWeight:"600"
    }
    const config = {
      reference:  Math.floor(Math.random() * 1000000000 + 1) + '-AUC',
      email: userEmail,
      amount: amount * 100,
      publicKey: 'pk_test_7140f98d7c52c6e559d00182fce18ec6f98443ae',
  };
  const onSuccess = (reference) => {
    // Implementation for whatever you want to do with reference and after success call.
    //console.log(reference);
    toaster('success', 'Payment Successful..Hold on while we verify payment.....', 2500);
    verifyPayment(reference.reference);
  };
  const onClose = () => {
    // implementation for  whatever you want to do when the Paystack dialog closed.
    console.log('closed')
  }
  const initializePayment = usePaystackPayment(config);
  const checkOut = () => {
    setSpinner(true)
      const payload = {
            email: userEmail,
            shoppingList:newData,
            totalCost: amount,
        }
        console.log(payload);
        const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify(payload)
        }
          // calling api using fetch
          // checkout api
        fetch(`${baseUrl}products/checkout`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===200){
                //console.log(data.transactionId);
                //setTransactionId(data.transactionId)
                sessionStorage.setItem('transactionId', data.transactionId)
                setSpinner(false)
                initializePayment(onSuccess, onClose)    
            }
            else{
                toaster('error', data.message, 2500);
                //console.log(data.message);
                setErrorMessage(data.message)
                setSpinner(false)
            }
            
        })
        .catch(err=>{
          toaster('error', err, 2500);
          setErrorMessage(err)
        })
  }

  const verifyPayment=(reference)=>{
    const payload = {
         referenceCode: reference,
         amount: amount,
         transactionId: sessionStorage.getItem('transactionId')  
        }
        console.log(payload);
    const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify(payload)
    }
    // calls verify payment api
        fetch(`${baseUrl}products/verifyPayment`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===200){
                //console.log(data);
                toaster('success', 'Payment verified. Thank you for your purchase', false);
                dispatch({
                  type:"EMPTY_BASKET"
                })
            }
            else{
                //console.log(data);
                toaster('error', data.message, 2500);
                setErrorMessage(data.message)
            }
            
        })
        .catch(err=>{
          toaster('error', err, 2500);
          setErrorMessage(err)
        })
  }
    return (
        <div className="checkout">
                 <ToastContainer/>
                <button style={styles} onClick={checkOut}>
              {spinner && ( <CircularProgress size="20px" />)}
              CHECKOUT
            </button>
        </div>
    )
}

export default Checkout
