import React from 'react'
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import './pagination.scss'

function Pagination({ totalPages, currentPage, setPage }) {
    let pages = []
   
    for (let i = 1; i <= totalPages; i++) {
        pages.push(i);
    }
       
        
      
  return (
    <div className="pagination">
      {currentPage > 1 && (
        <div className="previous">
          <NavigateBeforeIcon onClick={() => setPage(currentPage - 1)} />
        </div>
      )}

      <div className="pageNumbers">
        {pages.map((num) => (
          <div
            className={num === currentPage ? "number active" : "number"}
            key={num}
            onClick={() => setPage(num)}
          >
            {num}
          </div>
        ))}
      </div>
      {totalPages > 1 && (
        <div className="next">
          <NavigateNextIcon onClick={() => setPage(currentPage + 1)} />
        </div>
      )}
    </div>
  );
}

export default Pagination;