import React,{useState, useEffect, useMemo} from 'react'
import ReactApexChart from 'react-apexcharts'
import './piechart.scss'
import baseUrl from '../../helpers/baseUrl';
import axios from 'axios';
import Loader from 'react-js-loader'
import toaster from '../../helpers/toaster';

function Piechart() {
  
  const state = {
    series: [0,0,0],
    options: {
      chart: {
        width: 380,
        type: "donut",
      },
      legend: {
        position: "bottom",
      },
      stroke: {
        width: 0,
      },
      plotOptions: {
        pie: {
          donut: {
            labels: {
              show: true,
              total: {
                showAlways: true,
                show: true,
              },
            },
          },
        },
      },
      labels: ["Pending", "Paid", "Collected"],
      colors: ["#F1C247", "#EB4343", "#F1EFEF"],
      dataLabels: {
        dropShadow: {
          blur: 3,
          opacity: 0.8,
        },
      },
      fill: {
        type: "gradient",
        opacity: 1,
      },
      states: {
        hover: {
          filter: "none",
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200,
            },
            legend: {
              position: "bottom",
            },
          },
        },
      ],
    },
  };



  const [chartInfo, SetchartInfo] = useState(state);
  const [loading, setLoading] = useState(false)
 
  useEffect(() => {
    const fetchData = async () => {
      const url = baseUrl + "admin/users/records";
      setLoading(true);
      const {
        data: { data },
      } = await axios.get(url);
      const { paid, pending, collected } = data;
      setLoading(false);
      const series = [pending, paid, collected];
      SetchartInfo({ ...chartInfo, series });
    };
    fetchData().catch((error) => {
      setLoading(false)
      toaster("info", "Chart info not found", 2000)
    });
  }, []);

  return (
    <div className="piechat">
      <span className="piechat__description">Total Items Sold</span>
      {loading ? (
        <Loader
          type="hourglass"
          bgColor={"#000000"}
          title={"loading..."}
          color={"#000000"}
          size={100}
        />
      ) : (
        <ReactApexChart
          options={chartInfo.options}
          series={chartInfo.series}
          type="donut"
          width={380}
        />
      )}
    </div>
  );
}

export default Piechart