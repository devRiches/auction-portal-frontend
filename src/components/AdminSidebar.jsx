import React, {useState, useEffect} from 'react'
import { Link, useLocation } from "react-router-dom";
import dashboardIcon from "../images/dashboardIcon.svg";
import transactionIcon from '../images/transactionIcon.svg'
import settingIcon from '../images/settingIcon.svg'
import '../styles/adminSidebar.scss'

function AdminSidebar() {
  const [tab, setTab] = useState('/admin');
  const location = useLocation();
  useEffect(() => {
    setTab(location.pathname);
  }, [location]);
 

  return (
    <div className="admin-side-wrapper">
      <Link to="/admin" className={tab === "/admin" ? "dashboard-nav-item current" : "dashboard-nav-item"}>
        <span className="dashboard-nav__icon">
          <img src={dashboardIcon} alt="dashboardIcon" />{" "}
        </span>
        <span className="dashboard-nav__title"> Dashboard </span>
      </Link>

      <Link to="/admin/transactions" className={tab === "/admin/transactions" ? "dashboard-nav-item current" : "dashboard-nav-item"} >
        <span className="dashboard-nav__icon">
          <img src={transactionIcon} alt="dashboardIcon" />{" "}
        </span>
        <span className="dashboard-nav__title"> Transactions </span>
      </Link>

            
      <Link to="/admin/new-product" className={tab === "/admin/new-product" ? "dashboard-nav-item current" : "dashboard-nav-item"} >
        <span className="dashboard-nav__icon">
          <img src={transactionIcon} alt="dashboardIcon" />{" "}
        </span>
        <span className="dashboard-nav__title"> Contenet Manager </span>
      </Link>

      <Link to="#" className={tab === "settings" ? "dashboard-nav-item current" : "dashboard-nav-item"} >
        <span className="dashboard-nav__icon">
          <img src={settingIcon} alt="dashboardIcon" />{" "}
        </span>
        <span className="dashboard-nav__title"> Settings </span>
      </Link>
    </div>
  );
}

export default AdminSidebar;