import React, { useRef, useState } from "react";
import '../styles/overviewBar.scss'

function OverviewBar() {
    const selectRef = useRef(null);
    const [rangeOpen, setRangeOpen] = useState(false);

  const handleMouseDown = (e) => {
    e.preventDefault();
    const select = selectRef.current; //select-control
    const selector = select.children[0]; //select-input
    const dropDown = document.createElement("ul");

    dropDown.className = "selector-options";
    [...select.children[0]].forEach((option) => {
      const dropDownOption = document.createElement("li");
      dropDownOption.textContent = option.textContent;
      dropDown.appendChild(dropDownOption);

      dropDownOption.addEventListener("mousedown", (e) => {
        e.stopPropagation();
        setRangeOpen(false);
        selector.value = option.value;
        dropDown.remove();
      });
    });

    if (!rangeOpen) {
      select.appendChild(dropDown);
      setRangeOpen(true);
    } else {
      setRangeOpen(false);
      select.lastChild.remove();
    }
 };
    
    
  return (
    <div className="overview__bar">
      <div className="overview__title">Overview</div>
      <div
        className="dropdown__container"
        ref={selectRef}
        onMouseDown={handleMouseDown}
      >
        <select value={""}>
          <option value=""> Today </option>
          <option value="1"> Weekly </option>
          <option value="doc"> Monthly </option>
        </select>
      </div>
    </div>
  );
}

export default OverviewBar;
