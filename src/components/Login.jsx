import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import toaster from '../helpers/toaster';
import { ToastContainer } from 'react-toastify';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../styles/login.scss'
import baseUrl from '../helpers/baseUrl';
function Login(props) {

     const history = useHistory();
    //   const location = useLocation()
     const [input, setInput] = useState("")
     const [spinner, setSpinner] = useState(false)
     const [errorMessage, setErrorMessage] = useState('')
   
     const login = (e)=> {
        e.preventDefault();
        setSpinner(true);
        toaster('success','connecting...', 1000)

        // set payload
        const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify({ email: input })
        }
        // calling api using fetch
        fetch(`${baseUrl}users/login`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if (data.status === 200) {
                sessionStorage.setItem('userInfo', JSON.stringify(data.data))
                sessionStorage.setItem('email', data.data.email)
                sessionStorage.setItem('phoneNumber', data.data.phone)
                sessionStorage.setItem('fullName', data.data.fullname)
                sessionStorage.setItem("isLoggedIn", true);
                history.push("/products")
            }
            else{
                toaster('error', data.message, false);
                setErrorMessage(data.message)
                setSpinner(false)
            }
            
        })
            .catch(err => {
                toaster("error", "unable to reach the server", false);
                setErrorMessage(err.message)
                setSpinner(false);
            })

        //console.log(input);
        
        
        
     }
    return (
        <div className="login">
             <ToastContainer/>
            <h1>Log In</h1>
            <form className="login__form" onSubmit={login}>
                <div className="input__container">
                    <label>Company email</label>
                    <input type="email" required value={input} onChange={(e)=>{setInput(e.target.value)}} placeholder="name@giglogistics.com"/>
                </div>
                <button>
                    {spinner && ( <CircularProgress size="20px" />)}
                    LOGIN
                </button>
                 <div className="error" style={{color:'red',textAlign:"center"}}>
                        {errorMessage}
                 </div>
            </form>
           
            
        </div>
    )
}

export default Login
