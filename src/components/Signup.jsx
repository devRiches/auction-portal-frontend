import React, { useState } from 'react';
import { useHistory} from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress';
import PhoneInput from 'react-phone-number-input/input'
import '../styles/signup.scss'
import 'react-phone-number-input/style.css'
import baseUrl from '../helpers/baseUrl';


function Signup() {
    const initialValue = {
        emailAddress:"",
        userName: "",
        phoneNumber: "",
        
    }
    const history = useHistory()
    const [values, setValues] = useState(initialValue);
    const [number, setNumber] = useState('')
    const [errorMessage, setErrorMessage] = useState('');
    const [spinner, setSpinner] = useState(false)
    // handles change in input
    const handleChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value,
        });
    }
    
    // registers new user
    const registerUser = (e) => {
        e.preventDefault();
        setSpinner(true)
        const payload = {
            email: values.emailAddress,
            phone: number,
            fullname: values.userName
        }
        const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify(payload)
        }
        // calling api using fetch
          fetch(`${baseUrl}users/signup`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===201){
                console.log(data);
                sessionStorage.setItem('email', values.emailAddress)
                sessionStorage.setItem('phoneNumber', number)
                sessionStorage.setItem('fullName', values.userName)
                //sessionStorage.setItem("isLoggedIn", true);
                history.push("/otp")
            }
            else{
                //toaster('error', data.message, false);
                setErrorMessage(data.message)
                setSpinner(false)
            }
            
        })
        .catch(err=>{setErrorMessage(err)})
        
    }
    return (
         
        <div className="signup">
            <h1>Create account</h1>
            <form className="signup__form" onSubmit={registerUser}>
                 <div className="input__container">
                    <label>Company email</label>
                    <input type="text" required placeholder="name@giglogistics.com" 
                    name="emailAddress" value={values.emailAddress} onChange={handleChange}/>
                </div>
                 <div className="input__container">
                    <label>Full name</label>
                    <input type="text" required placeholder="first and last name" 
                    name="userName" value={values.userName} onChange={handleChange}/>
                </div>
                 <div className="input__container">
                    <label>Phone number</label>
                       <PhoneInput
                            country="NG"
                            placeholder="Enter phone number"
                            value={number}
                            onChange={setNumber}/>
                </div>
                <button>
                    {spinner && ( <CircularProgress size="20px" />)}
                    CREATE ACCOUNT
                </button>
                <div className="error" style={{color:'red',textAlign:"center"}}>
                        {errorMessage}
                </div>
            </form>
           
        </div>
    )
}

export default Signup
