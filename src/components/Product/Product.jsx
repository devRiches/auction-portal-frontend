import React, { useState, lazy, Suspense } from 'react'
import toaster from '../../helpers/toaster';
import CurrencyFormat from 'react-currency-format';
import { useStateValue } from '../../helpers/stateProvider';
import Loader from "react-js-loader";
import './product.scss'
const ProductImage = lazy(() => import("../ProductImage/ProductImage"));

function Product({sku, price, productName,itemDescription,itemQuantity,image,id}) {
    const [{basket}, dispatch] = useStateValue();
    const [quantity, setQuantity] = useState(1);



    const handleChange = (e) => {
        if(quantity >= itemQuantity){
                alert('too much');
                setQuantity(1);
                return
        }
       
        setQuantity(parseInt(e.target.value))
            
    }
    const addToBasket = () => {  
          if(quantity < 1){
              toaster('error', 'Invalid Quantity!', 3000)
              setQuantity(1)
          } else {
            dispatch({
              type: "ADD_TO_BASKET",
              item: {
                id: id,
                price: price,
                quantityValue: quantity,
                image: image,
                name: productName,
                sku: sku,
              },
            });
            toaster("success", "Added to Cart!", 2000);
          }
    }
    const increment = () => {
        if(quantity >= itemQuantity){
            toaster('error', 'Exceeds available value', 3000)
            setQuantity(1);
            return
        }
        else{
            setQuantity(quantity+1)
        }
        
    }
    const decrement = () => {
         if(quantity < 1 || quantity === 0 ){
            setQuantity(1);
            return
        }
        else{
            setQuantity(quantity-1)
        }
        
    }
    return (
      <div className="product">
        <div className="products__info">
          <Suspense
            fallback={
              <Loader
                bgColor={"#000000"}
                title={"Loading Image"}
                size={50}
              />
            }
          >
            <ProductImage image={image} />
          </Suspense>
          <div className="productName__price">
            <div className="product__name">{productName}</div>
            <div className="product__price">
              <CurrencyFormat
                value={price}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"₦"}
              />
            </div>
          </div>
          <div className="product__description">{itemDescription}</div>
          <div className="product__quantity">
            <div className="product__number">
              {itemQuantity} {itemQuantity > 1 ? "items" : "item"} left
            </div>
            <div className="product__value">
              <span onClick={decrement}>-</span>
              <input type="number" value={quantity} onChange={handleChange} disabled/>
              <span onClick={increment}>+</span>
            </div>
          </div>
        </div>

        <div className="cart__button">
          {itemQuantity < 1 ? (
            <button className="button__inCart" disabled={true}>
              <i className="fas fa-shopping-cart"></i> OUT OF STOCK{" "}
            </button>
          ) : (
            <button onClick={addToBasket}>
              {" "}
              <i className="fas fa-shopping-cart"></i> ADD TO CART
            </button>
          )}
        </div>
      </div>
    );
}

export default Product
