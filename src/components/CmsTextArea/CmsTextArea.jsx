import React from 'react'

import './cmsTextArea.scss'
function CmsTextArea({ id, value, handler }) {
  return (
    <div className="cms_textArea">
      <textarea id={id} value={value} rows="5" onChange={(e) => handler(e)} />
    </div>
  );
}

export default CmsTextArea