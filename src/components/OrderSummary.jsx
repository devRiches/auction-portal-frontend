import React from 'react'
import CurrencyFormat from 'react-currency-format';
import { getBasketTotal } from '../helpers/reducer';
import { useStateValue } from '../helpers/stateProvider'
import '../styles/orderSummary.scss'
import Checkout from './Checkout';
function OrderSummary() {
    const [{ basket }] = useStateValue();
    return (
        <div className="orderSummary">
            <div className="orderSummary__header">Order Summary</div>
            <div className="orderSummary__subtotal">
                <span style={{color:"#A7A7A7"}}>Subtotal</span>
                <span><CurrencyFormat   value={getBasketTotal(basket)}
                                        displayType={'text'}
                                        thousandSeparator={true} 
                                        prefix={'₦'} />
                </span>
            </div>
            <div className="orderSummary__subtotal" style={{fontSize:"1.3rem"}}>
                <span>Total</span>
                <span><CurrencyFormat   value={getBasketTotal(basket)}
                                        displayType={'text'}
                                        thousandSeparator={true} 
                                        prefix={'₦'} />
                </span>
            </div>
            <div className="orderSummary__checkout">
                <Checkout amount={getBasketTotal(basket)}/>
            </div>
        </div>
    )
}

export default OrderSummary
