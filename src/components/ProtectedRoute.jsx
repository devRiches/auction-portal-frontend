import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import useAuth from '../helpers/useAuth'

function ProtectedRoute({ component: Component, role, ...rest }) {
  const loggedInUser = useAuth();
    return (
        <Route {...rest}
            render={(props) => {
                if (loggedInUser) {
                    return role === loggedInUser.role || role === 'user' ? (
                      <Component {...props} />
                    ) : (
                      <Redirect
                        to={{ path: "/", state: { from: props.location } }}
                      />
                    );
                }

                return (<Redirect to={{ path: "/", state: { from: props.location } }} />)
          }}
          
        />
    );
}

export default ProtectedRoute