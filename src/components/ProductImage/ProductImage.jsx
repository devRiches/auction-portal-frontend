import React from 'react'
import {Image} from 'cloudinary-react'
import LazyLoad from 'react-lazy-load'
import './productImage.scss'

function ProductImage({image}) {
  return (
    <div className="product__image">
      <LazyLoad>
      <img src={image} alt="product" />
      {/* <Image cloudName="gig-logistics-it" publicId={image} /> */}
      </LazyLoad>
    </div>
  );
}

export default ProductImage