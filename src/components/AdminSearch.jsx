import React, {useState, useRef} from "react";
import { DateRangePicker } from "materialui-daterange-picker";
import arrowRight from "../images/arrow-right.svg";
import calenderIcon from "../images/calenderIcon.svg";
import { useAdminDataContext } from "../helpers/adminDataProvider";
import axios from "axios";
import baseUrl from "../helpers/baseUrl";
import toaster from "../helpers/toaster";
import * as XLSX from 'xlsx'
import "../styles/adminSearch.scss";

function AdminSearch() {
  const [open, setOpen] = useState(false);
  const [dateRange, setDateRange] = useState({});
  const [exportOpen, setExportOpen] = useState(false)
  const [searchEmail, setSearchEmail] = useState("")
  const { adminData, setAdminData, setLoading, setTab } = useAdminDataContext();

  const selectRef = useRef(null)

  const toggle = () => setOpen(!open);
  


  const onDownload = () => {
    const workSheet = XLSX.utils.json_to_sheet(adminData);
    const workBook = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(workBook, workSheet, 'Auction Transactions')

    //Buffer
    let buf = XLSX.write(workBook, { bookType: "xlsx", type: "buffer" })
    //Binary string
    XLSX.write(workBook, { bookType: "xlsx", type: "binary" })
    
    //Download file
    XLSX.writeFile(workBook, "Auction_Transactions.xlsx");
  }

  
  const handleMouseDown = (e) => {
    e.preventDefault();
    const select = selectRef.current;   //select-control
    const selector = select.children[0];  //select-input
    const dropDown = document.createElement("ul");

    dropDown.className = "selector-options";
    [...select.children[0]].forEach((option) => {
      const dropDownOption = document.createElement("li");
      dropDownOption.textContent = option.textContent;
      dropDown.appendChild(dropDownOption);

      dropDownOption.addEventListener("mousedown", (e) => {
        e.stopPropagation();
        setExportOpen(false)
        selector.value = option.value;
        dropDown.remove();
        onDownload()
      });
    });
    
    if (!exportOpen) {
      select.appendChild(dropDown);
      setExportOpen(true)
    } else {
      setExportOpen(false);
      select.lastChild.remove()
      
    }
    

  }

  const submitSearch = async(e) => {
    e.preventDefault()
    setLoading(true)
    const url = baseUrl + "admin/user/transacttions";
    const payload = { "email": searchEmail }
    setSearchEmail("");

    try {
      const {data} = await axios.post(url, payload);
      if (data.status === 200) {
        setAdminData(data.data)
        setLoading(false)
      } else {
        setLoading(false);
        toaster("error", data.message, false);
      }

    } catch (error) {
      setLoading(false);
      toaster("error", 'unable to fetch new data, please enter a valid email', false);
    }
  }


  const handleDateRange = async (range) => {
    toggle();
    setLoading(true);
    const { startDate, endDate } = range;
    let start = new Date(startDate);
    let end = new Date(endDate);
    const url = baseUrl + `admin/users/transactions?startDate=${start}&endDate=${end}`;
    try {
      const { data } = await axios.get(url);

      if (data.status === 200) {
        setAdminData(data.data);
        setTab('all');
        setLoading(false);
        return
      }
      setLoading(false);
      throw new Error('Failed to fetch data')
    } catch (error) {
      setLoading(false);
      toaster("error", "Failed to fetch data", false);
    }

  };
 


  return (
    <div className="admin-search-wrapper">
      <form
        className="admin-search-wrapper__search"
        onSubmit={(e) => submitSearch(e)}
      >
        <i className="fas fa-search"></i>
        <input
          type="text"
          placeholder="Search"
          value={searchEmail}
          onChange={(e) => setSearchEmail(e.target.value)}
        />
      </form>

      <div className="admin-search-wrapper__filters">
        <div className="search-date-settings">
          <div className="date-input-controls" onClick={toggle}>
            <label className="start-date">{"Start date"}</label>
            <label className="arrow-right">
              <img src={arrowRight} alt={"arrow"} />
            </label>
            <label className="end-date">End date</label>
            <label className="calender">
              <img src={calenderIcon} alt={"calender Icon"} />
            </label>
          </div>

          <DateRangePicker
            open={open}
            toggle={toggle}
            wrapperClassName={"dateRangePicker"}
            onChange={(range) => handleDateRange(range)}
          />
        </div>

        <div
          className="select-input-control"
          ref={selectRef}
          onMouseDown={handleMouseDown}
        >
          <select value={""}>
            <option value=""> Export </option>
            <option value="xlsx"> Export as .xlsx </option>
            <option value="doc"> Export as .doc </option>
          </select>
        </div>
      </div>
    </div>
  );

  
}

export default AdminSearch;
