import React, { useEffect, useState } from 'react'
import { useStateValue } from '../../helpers/stateProvider';
import Loader from "react-js-loader";
import axios from "axios";
import 'react-toastify/dist/ReactToastify.css';
import Product from '../Product/Product';
import { ToastContainer } from 'react-toastify';
import ScrollToTop from "react-scroll-to-top";
import baseUrl from '../../helpers/baseUrl';
import './products.scss'
import toaster from '../../helpers/toaster';
import Pagination  from '../Pagination';

function Products() {
 
    const [{category}] = useStateValue();
    const [loading, setLoading] = useState(true);
    const [search, setSearch] = useState('')
    const [data, setData] = useState([]);
    const [page, setPage] = useState(1);
    const [totalPages, setTotalpages] = useState(1);

    
    
    //const [search, setSearch] = useState('');
    useEffect(() => {
      setLoading(true);
      if (category.name === "ALL") {
        axios
          .get(`${baseUrl}products/?page=${page}`)
          .then((response) => {
            if (response.data.status === 200) {
              setLoading(false);
              console.log(response.data);
              const { products, Pages } = response.data;
              setData(products);
              setTotalpages(Pages);
            } else {
              toaster("error", response.message, false);
            }
          })
          .catch((err) => {
            toaster("error", err, false);
          });
      } else {
        axios
          .get(`${baseUrl}categories/${category.categoryId}`)
          .then((response) => {
            if (response.data.status === 200) {
              setLoading(false);
              setData(response.data.products);
            } else {
              toaster("error", response.message, false);
            }
          })
          .catch((err) => {
            toaster("error", err, false);
          });
      }
    }, [category, page]);
    
    const searchForProducts = () => {
        setLoading(true)
        axios.get(`${baseUrl}products/search?query=${search}`).then(
            response=>{
                  if(response.data.status===200){
                            setLoading(false)
                            setData(response.data.products)
                        }
                        else{
                            setLoading(false)
                            toaster('error', response.message, false)
                        } 
                console.log(response.data);
            }
        )
        .catch(err=>{toaster('error', err, false)})
    }

    
    return (
      <div className="products__container">
        <div className="search__input">
          <div className="search">
            <i className="fas fa-search"></i>
            <input
              type="text"
              placeholder="Search products, brands and categories"
              onChange={(e) => {
                setSearch(e.target.value);
              }}
            />
          </div>

          <button onClick={searchForProducts}>SEARCH</button>
        </div>
        {!loading ? (
          <div>
            <div className="products__header">{category.name}</div>
            <div className="products__content">
              <ScrollToTop smooth top={80} />
              <ToastContainer />
              {data.map((item) => (
                <Product
                  key={item._id}
                  sku={item.sku}
                  productName={item.name}
                  itemDescription={item.item_description}
                  price={item.price}
                  itemQuantity={item.quantity}
                  image={item.imgUrl}
                  id={item._id}
                />
              ))}
              {data.length === 0 && (
                <div className="noProduct">No Products found</div>
              )}
            </div>

            <Pagination
              currentPage={page}
              totalPages={totalPages}
              setPage={setPage}
            />
          </div>
        ) : (
          <div className="overlays">
            <div className="center">
              <Loader
                type="spinner-circle"
                bgColor={"#000000"}
                title={"Getting Products"}
                color={"#000000"}
                size={150}
              />
            </div>
          </div>
        )}
      </div>
    );
}

export default Products
