import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import { useStateValue } from '../helpers/stateProvider';
import gadget from '../images/gadgets.png'
import axios from "axios";
import baseUrl from '../helpers/baseUrl';
import { useEffect, useState } from 'react';
import Loader from "react-js-loader";

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function MobileSidebar() {
  const classes = useStyles();
   const [{}, dispatch] = useStateValue();
    const [productCategory, setCategory] = useState([])
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const allProducts = {
        categoryName:'ALL',
        categoryId:'1220399j3u8833'
    }
    // gets categories
    useEffect(() => {
        axios.get(`${baseUrl}categories`)
        .then(
            res=>{
                if(res.data.status===200){
                     setLoading(false)
                     setCategory(res.data.data)
                }
                else{
                    setLoading(false)
                    setError(res.message)
                }
               
                // console.log(res.data);
                
            }
        )
        .catch(
            err=>{
                setLoading(false)
                setError(err)
            }
        )
    }, [])
    // sets category type
    const typeOfProduct = (type) => {
            // setProductCategory(type)
            dispatch({
            type: 'PRODUCT_CATEGORY',
            category: {
                name:type.categoryName,
                categoryId:type.categoryId
            }
        })
        // console.log(type);
    }
    // responsible for mobile side bar
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
// toggles sidebar
  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
              {!loading ? (
                <ul className="sidebar__links">
                    <li onClick={()=>typeOfProduct(allProducts)} className="link">
                            <img src={gadget} alt="icon_image"/> ALL PRODUCTS
                        </li>
                    {productCategory.map(category=>(
                        <li onClick={()=>typeOfProduct(category)} className="link" key={category._id}>
                            <img src={gadget} alt="icon_image"/> {category.categoryName}
                        </li>
                    ))}
               
                </ul>
            ): (
                <div className="center">
               <Loader type="spinner-circle" bgColor={"#000000"} title={"Getting categories"} color={"#000000"}  size={110} />
            </div> 
            )}
    </div>
  );

  return (
    <div>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
            <span onClick={toggleDrawer(anchor, true)}><MenuIcon style={{color:"#fff"}}/></span>
          <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
