import "./cmsInput.scss";

function CmsInput({ id, placeholder, value, handler }) {
  return (
    <div className="cms_input">
      <input
        type="text"
        id={id}
        placeholder={placeholder}
        value={value}
        onChange={(e) => handler(e)}
      />
    </div>
  );
}

export default CmsInput;
