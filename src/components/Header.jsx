import React, { useState } from 'react'
import logo from '../images/image 2.png'
import '../styles/header.scss'
import { useStateValue } from '../helpers/stateProvider';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useHistory, Link } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MobileSidebar from './MobileSidebar'
import useAuth from '../helpers/useAuth';
function Header() {
    const [anchorEl, setAnchorEl] = useState(null)
    const loggedIn = sessionStorage.getItem("isLoggedIn");
    const userName = sessionStorage.getItem("fullName");
    const loggedInUser = useAuth();
    const history = useHistory()
    const [{basket}, dispatch] = useStateValue();
 
    // opens popup
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    }
    // closes popup
    const handleClose = () => {
    setAnchorEl(null);
  
  };
  const logout = () => {
      dispatch({ type:"EMPTY_BASKET"})
        sessionStorage.clear()
        history.push("")

  }
   
    const cart = () => {
        history.push("/cart")
    }
    return (
      <div className="header">
        {loggedIn ? (
          <div className="loggedIn">
            <div className="mobileSibebar">
              <MobileSidebar />
            </div>
            <div className="image">
              <Link to="/products">
                <img src={logo} alt="gig-logo" />
              </Link>
            </div>
            {/* <div className="search">
                    <i class="fas fa-search"></i>
                    <input  type="text"
                            placeholder="Search products, brands and categories"
                            onKeyUp={handleChange} 
                        />
                </div> */}
            <div className="icons">
              <div
                className="user__icon"
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleMenu}
              >
                {userName[0]}
              </div>

              <span className='username'>{userName}</span>

              <span
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleMenu}
                style={{ cursor: "pointer" }}
              >
                <ExpandMoreIcon style={{ color: "#fff" }} fontSize="large" />
              </span>

              <div className="cart__icon" onClick={cart}>
                <i className="fas fa-shopping-cart"></i>
              </div>
              <span className="cart__length">{basket.length}</span>
              {/* <div className="logout__icon" onClick={logout}>
                         <ExitToAppIcon/>  
                    </div> */}
            </div>
          </div>
        ) : (
          <div className="logo">
            <img src={logo} alt="gig-logo" />
          </div>
        )}
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={logout}>
            <ExitToAppIcon /> Logout
          </MenuItem>

          {loggedInUser && loggedInUser.role === "admin" ? (
            <MenuItem onClick={() => history.push("/admin")}>
              <ExitToAppIcon /> Dashboard
            </MenuItem>
          ) : (
            ""
          )}
        </Menu>
      </div>
    );
}

export default Header
