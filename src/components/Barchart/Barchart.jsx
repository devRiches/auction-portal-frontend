import React, {useState, useEffect} from 'react'
import ReactApexChart from 'react-apexcharts'
import './barchart.scss'
import baseUrl from "../../helpers/baseUrl";
import axios from "axios";
import Loader from "react-js-loader";
import toaster from "../../helpers/toaster";

const state = {
  series: [
    {
      data: [0,0,0,0,0,0,0],
    },
  ],
  options: {
    chart: {
      type: "bar",
      height: 350,
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    colors: ["#47B275"],
    xaxis: {
      categories: [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      ],
    },
  },
};

function Barchart() {
  const [chartInfo, SetchartInfo] = useState(state);
  const [loading, setLoading] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0)
  
  useEffect(() => {
    const today = new Date()
    const date = today.getDate();
    const day = today.getDay();
    const firstDayOfTheWeek = new Date(new Date().setDate(date - day))

    const fetchData = async () => {
      const url =
        baseUrl +
        `admin/users/transactions?startDate=${firstDayOfTheWeek}&&endDate=${today}`;
      
      setLoading(true)
      const {
        data: { data },
      } = await axios.get(url);

      const dateAmount = {};

      for (let i = 0; i < data.length; i++){
          let item = data[i]
          if (dateAmount[item.date]) {
            dateAmount[item.date] += item.price
          }else{
            dateAmount[item.date] = item.price
        }
      }
      let days = [0,0,0,0,0,0,0]
      for (const [key, value] of Object.entries(dateAmount)) {
        const day = new Date(key).getDay()
        days[day] = value
      }

      const total = days.reduce((acc, curr) => acc + curr, 0)
      setTotalAmount(total)

      const series = [
        {
          data: days,
        },
      ];
      SetchartInfo({ ...chartInfo, series });
      setLoading(false);
      
    }


    
    fetchData().catch((error) => {
      setLoading(false);
      toaster("info", "Bar Chart info not found", 2000);
    });

  },[])

  return (
    <div className="barchart">
      <span className="barchart__title">Payments</span>

      {loading ? (
        <Loader
          type="hourglass"
          bgColor={"#000000"}
          title={"loading..."}
          color={"#000000"}
          size={100}
        />
      ) : (
        <>
          <div className="total">₦{totalAmount}</div>
          <ReactApexChart
            options={chartInfo.options}
            series={chartInfo.series}
            type="bar"
            height={300}
            width={350}
          />
        </>
      )}
    </div>
  );
}

export default Barchart