import React from 'react'
import { useStateValue } from '../helpers/stateProvider';
import gadget from '../images/gadgets.png';
import Loader from "react-js-loader";
import axios from "axios";
import baseUrl from '../helpers/baseUrl';
import '../styles/sidebar.scss'
import { useEffect, useState } from 'react';

function Sidebar() {
    const [{}, dispatch] = useStateValue();
    const [productCategory, setCategory] = useState([])
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const allProducts = {
        categoryName:'ALL',
        categoryId:'1220399j3u8833'
    }
    useEffect(() => {
        axios.get(`${baseUrl}categories`)
        .then(
            res=>{
                if(res.data.status===200){
                     setLoading(false)
                     setCategory(res.data.data)
                }
                else{
                    setLoading(false)
                    setError(res.message)
                }
               
                // console.log(res.data);
                
            }
        )
        .catch(
            err=>{
                setLoading(false)
                setError(err)
            }
        )
    }, [])
    const typeOfProduct = (type) => {
            // setProductCategory(type)
            dispatch({
            type: 'PRODUCT_CATEGORY',
            category: {
                name:type.categoryName,
                categoryId:type.categoryId
            }
        })
    }
    return (
        <div className="sidebar__container">
            {!loading ? (
                <ul className="sidebar__links">
                    <li onClick={()=>typeOfProduct(allProducts)} className="link">
                            <img src={gadget} alt="icon_image"/> ALL PRODUCTS
                        </li>
                    {productCategory.map(category=>(
                        <li onClick={()=>typeOfProduct(category)} className="link" key={category._id}>
                            <img src={gadget} alt="icon_image"/> {category.categoryName}
                        </li>
                    ))}
               
                </ul>
            ): (
                <div className="center">
               <Loader type="spinner-circle" bgColor={"#000000"} title={"Getting categories"} color={"#000000"}  size={110} />
            </div> 
            )}
            {
                error && ( <div className="center" style={{color:'red'}}>
                                {error}
                            </div>)
            }
           
            
          
        </div>
    )
}

export default Sidebar
