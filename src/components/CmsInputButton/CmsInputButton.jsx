
import './CmsInputButton.scss'
function CmsInputButton({label, icon,loader, handler}){
    
    return(
        <div className="cms_input_button">
            <button onClick={(e) => handler(e)}>
                {loader}
                {icon && (<img src={icon} alt="Icon" />)}
                {label}
            </button>
        </div>

    );
}

export default CmsInputButton