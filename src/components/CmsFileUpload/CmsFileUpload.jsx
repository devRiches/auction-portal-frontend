import React, { useEffect, useState } from 'react'

import './cmsFileUpload.scss'

function CmsFileUpload({ label, id, value, setUrl }) {
    const [file, setFile] = useState('')
    const [err, setErr] = useState('')
    const [preview, setPreview] = useState(null)


    useEffect(() => {
        if (!file || !value) {
          setPreview(null)
          setFile(null);
            setUrl(null)
            return
        }
        // const filePath = URL.createObjectURL(file);
        const reader = new FileReader()
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          setPreview(reader.result)
          setUrl(reader.result)

        }
        
        return () => URL.revokeObjectURL(file);
    }, [file,value, setUrl])

    const handleChange = (event) => {
        const path = Array.from(event.target.files)[0];
        const allowedExt = ['jpg', 'jpeg', 'png']
        const userfile = path.name
        const ext = userfile.split(".").pop();
        const wrongFormat = allowedExt.indexOf(ext)
        if (wrongFormat === -1) setErr('only images are allowed');

      setFile(() => path);
      setUrl(() => path)

    }


    const removeImage = ()=> {
        setFile(() => null);
      setPreview(null);
      setUrl(null)
    }




    return (
      <div className="cms_file_upload">
        <span>Upload image(s)</span>
        <div className="cms_file_cover">
          {file ? (
            <img src={preview} alt={"img"} />
          ) : (
            [
              <label htmlFor={id}>{label}</label>,
              <input
                type="file"
                id={id}
                value={value}
                accept="image/*"
                onChange={(e) => handleChange(e)}
                hidden
              />,
              <span>Accepts JPG, PNG and WebP formats</span>,
            ]
          )}
        </div>
            {err && <span>{err}</span>}
            {preview && (<div className='file_info'><span>{ file.name }</span> <span className='remove_btn' onClick={removeImage}>Remove</span></div>)}
      </div>
    );
}

export default CmsFileUpload