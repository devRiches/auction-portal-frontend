import React, { useEffect, useState } from "react";
import { DataGrid } from "@material-ui/data-grid";
import { useAdminDataContext } from "../../helpers/adminDataProvider";
import { ToastContainer } from "react-toastify";
import baseUrl from "../../helpers/baseUrl";
import toaster from "../../helpers/toaster";
import axios from "axios";
import _ from "lodash";
import Loader from "react-js-loader";
import "./adminTable.scss";



function AdminTable({page}) {
  const { adminData, setAdminData, loading, setLoading, tab, setTab } =
    useAdminDataContext();
  

  const columns = [
    { field: "transactionId", headerName: "Transaction ID", width: 200 },
    {
      field: "customer",
      headerName: "Customer Name",
      width: 200,
      editable: false,
    },
    {
      field: "email",
      headerName: "Email",
      width: 200,
      editable: false,
    },
    {
      field: "item_name",
      headerName: "Order",
      width: 200,
      editable: false,
    },
    {
      field: "quantity",
      headerName: "Quantity",
      width: 200,
    },
    {
      field: "date",
      headerName: "Date",
      width: 200,
      renderCell: (params) => {
        return (
          <span> {new Date(params.value).toISOString().split("T")[0]} </span>
        );
      },
    },
    {
      field: "price",
      headerName: "Price",
      width: 200,
    },
    {
      field: "status",
      headerName: "Status",
      width: 200,
      editable: true,
      type: "singleSelect",
      valueOptions: ["pending", "paid", "collected"],
      renderCell: (params) => {
          let value = params.value
        return <span className={value==='pending' ? 'redField' : "greenField"}> {value} </span>;
      },
    },
  ];

  const httpCall = async () => {
    const url =
      page === "admin"
        ? baseUrl + "admin/users/paid-transactions"
        : baseUrl + "admin/users/transactions";
    const { data } = await axios.get(url);
    return data;
  };

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        
        const data = await httpCall();
        if (data.status === 200) {
          const Details = data.data;
          setAdminData(() => Details);
          setLoading(false);
        } else {
          setLoading(false);
          toaster("error", data.message, false);
        }
      } catch (error) {
        setLoading(false);
        toaster("error", "table data not found", 2000);
      }
    };
    fetchData();
  }, [page, setAdminData, setLoading]);



  const handleFilter = async (e) => {
    setLoading(true);
    const name = e.target.getAttribute("name");
    setTab(name);
    const data = await httpCall();
    if (data.status === 200) {
      const Details = data.data;
      if (name === "all") {
        setAdminData(() => Details);
        setLoading(false);
        return;
      }
      const filtered = Details.filter((element) => element.status === name);
      setAdminData(() => filtered);
      setLoading(false);
    } else {
      setLoading(false);
      toaster("error", data.message, false);
    }
  };
    
    
    const handleRowEditCommit = async (params, event) => {
        let [row] = adminData.filter((data) => data.uniqueId === params.id);
        const status = event.target.value
        const url = baseUrl + "admin/transaction/product/update";
        const {transactionId, id} = row
        const payload = {
          status,
            transactionId,
          productId: id
        };

        try {
            const { data } = await axios.post(url, payload);
            if (data.status === 200) { 
                toaster("success", "product updated successfully", false);
                return
            }
            toaster("error", data.message, false);
        } catch (error) {
            toaster("error", error.message, false);
        }
        
   }

  return (
    <div className="admin-table-wrapper" style={page==='admin'? {marginTop:0}:{}}>
      <ToastContainer />
      { page !=='admin' && (<nav>
        <div
          className={"nav-category" + (tab === "all" ? " active" : "")}
          name="all"
          onClick={(e) => handleFilter(e)}
        >
          All orders
        </div>
        <div
          className={"nav-category" + (tab === "paid" ? " active" : "")}
          name="paid"
          onClick={(e) => handleFilter(e)}
        >
          Paid
        </div>
        <div
          className={"nav-category" + (tab === "pending" ? " active" : "")}
          name="pending"
          onClick={(e) => handleFilter(e)}
        >
          Pending
        </div>
      </nav>
      )}
      <div className="admin-table-data" style={page === 'admin' ? { marginTop: 0 } : {}}>
        {loading ? (
          <Loader
            type="spinner-circle"
            bgColor={"#000000"}
            title={"Getting Records"}
            color={"#000000"}
            size={150}
          />
        ) : (
        <DataGrid
            columns={columns}
            rows={adminData}
            getRowId={(row) => row.uniqueId}
            pageSize={10}
            checkboxSelection
            rowsPerPageOptions={[10]}
            onCellEditCommit={handleRowEditCommit}
            isCellEditable={(params) => params.row.status !== "pending"}
            experimentalFeatures={{ newEditingApi: true }}
          />
        )}
      </div>
    </div>
  );
}

export default AdminTable;
