import React from 'react'
import { useStateValue } from '../helpers/stateProvider'
import '../styles/mobilecart.scss'
import OrderSummary from './OrderSummary';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { IconButton } from '@material-ui/core';
import CurrencyFormat from 'react-currency-format';

function MobileCart() {
    const [{ basket }, dispatch] = useStateValue();
    const deleteItem = (id) => {
            dispatch({
                type:"REMOVE_FROM_BASKET",
                id:id
            })
    }
    return (
        <div className="mobile__cart">
            <div className="mobile__cartHeader">Order Summary</div>
            {basket.map(item=>(
                <div className="mobile__cartProducts" key={item.id}>
                    <div className="mobile__image">
                        <img src={item.image} alt="checkout__logo" />
                    </div>
                    <div className="mobile__details">
                        <div className="item__name">{item.name}</div>
                        <div className="item__price"><CurrencyFormat   value={item.price}
                                                                        displayType={'text'}
                                                                        thousandSeparator={true} 
                                                                        prefix={'₦'} />
                        </div>
                    </div>
                    <div className="mobile__quantity">
                        <span>{item.quantityValue}</span>
                    </div>
                    <div className="delete">
                        <span onClick={()=>{deleteItem(item.id)}}> 
                            <IconButton>
                                <DeleteForeverIcon/>
                            </IconButton>
                        </span>
                    </div>
                </div>
            ))}
            {basket.length === 0 && (<div className="empty__cart">Cart is Empty</div>)}
            <div className="mobile__cartSummary">
                <OrderSummary/>
            </div>
        </div>
    )
}

export default MobileCart
