import "./CmsSelectInput.scss";
function CmsSelectInput({ id, placeholder, value, data, handler }) {
  return (
    <div className="cms_select_input">
      <select id={id} placeholder={placeholder} value={value} onChange={(e) => handler(e)}>
      <option value=""> Select Category</option>
        {data.map((option) => (
          <option key={option.categoryId} value={option.categoryId}>
            {option.categoryName}
          </option>
        ))}
      </select>
    </div>
  );
}

export default CmsSelectInput;
