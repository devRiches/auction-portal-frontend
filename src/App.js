import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Redirect } from 'react-router'
import './App.css';
import MarketPlace from "./views/MarketPlace";
import LoginSignup from './views/LoginSignup';
import Otp from './views/Otp';
import Cart from './views/Cart'
import AdminDashboard from "./views/AdminDashboard/AdminDashboard";
import AdminTransactionDashboard from "./views/AdminTransactions/AdminTransactionDashboard";
import ProtectedRoute from "./components/ProtectedRoute"
import ContentManager from './views/cms/ContentManager';


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/user" />
          </Route>
          <ProtectedRoute path="/admin" exact component={AdminDashboard} role="admin" />
          <ProtectedRoute path="/admin/transactions" component={AdminTransactionDashboard} role="admin" />
          <ProtectedRoute path="/admin/new-product" component={ContentManager} role="admin"/>
          <Route path="/user">
            <LoginSignup />
          </Route>
          <ProtectedRoute path="/products" component={MarketPlace} role="user" />
         
          <Route path="/otp">
            <Otp />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
        </Switch>
      </Router>
      {/* <Dashboard /> */}
    </div>
  );
}


export default App;
