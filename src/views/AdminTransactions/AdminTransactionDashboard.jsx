import React from "react";
import AdminSidebar from "../../components/AdminSidebar";
import Header from "../../components/Header";
import AdminSearch from "../../components/AdminSearch";
import AdminTable from "../../components/AdminTable/AdminTable";
import "../AdminDashboard/adminPages.scss";

function AdminTransactionDashboard() {
  console.log('transaction')
  return (
    <div className="admin-dashboard-page">
      <Header />
      <div className="admin-dashboard-body">
        <AdminSidebar />
        <div className="admin-dashboard-body__data">
          <AdminSearch />
          <AdminTable page='transactions'/>
        </div>
      </div>
    </div>
  );
}

export default AdminTransactionDashboard;
