import React, { useState,useEffect, Suspense } from 'react'
import { Switch, Route, Link, useLocation } from 'react-router-dom'
// import { CSSTransition, SwitchTransition } from 'react-transition-group'
import Header from '../components/Header';
import Login from '../components/Login';
import Signup from '../components/Signup';
import Loader from "react-js-loader";
import '../styles/transition.scss'
import '../styles/loginContainer.scss'


function LoginSignup() {
     const location = useLocation();
     useEffect(() => {
      console.log(location);
      // This code will run when the component mounts
      const fetchData = async () => {
        try {
          const url = "https://gig-auction.onrender.com/"; // Replace with your API endpoint
          const resp = await fetch(`${url}`);
          const data = await resp.text();
          console.log(data);
        } catch (error) {
          console.log(error);
        }
      };
  
      fetchData();
    }, [location]);
     //const select = localStorage.getItem("selection")
     const [select, setSelect] = useState(0)
     const toggleSelect = (x) => {
         setSelect(x)
        localStorage.setItem("selection", x)
     }
    return (
      <div className="container">
        <Header />
        <div className="content__container">
          <Suspense
            fallback={
              <Loader bgColor={"#000000"} title={"Loading Image"} size={100} />
            }
          >
            <div className="image__container">
              {/* <img src={logo} alt="test" /> */}
            </div>
          </Suspense>
          <div className="content">
            <div className="links">
              <div>
                <Link
                  to="/user/signup"
                  className={
                    select === 1 ? "selected__link" : "unselected__link"
                  }
                  onClick={() => {
                    toggleSelect(1);
                  }}
                >
                  Register
                </Link>
              </div>
              <div>
                <Link
                  to="/user"
                  className={
                    select === 0 ? "selected__link" : "unselected__link"
                  }
                  onClick={() => {
                    toggleSelect(0);
                  }}
                >
                  Log In
                </Link>
              </div>
            </div>
            <div className="routes">
              {/* <SwitchTransition mode={'out-in'}>
                        <CSSTransition
                            timeout={400 }
                            classNames='transition'
                            key={location.hash}
                        > */}

              <Switch location={location}>
                <Route path="/user/signup">
                  <Signup />
                </Route>
                <Route path="/user">
                  <Login />
                </Route>
              </Switch>

              {/* </CSSTransition>
                    </SwitchTransition> */}
            </div>
          </div>
        </div>
      </div>
    );
}

export default LoginSignup
