import { useState, useEffect } from "react";
import axios from "axios";
import Header from "../../components/Header";
import { ArrowBack } from "@material-ui/icons";
import CmsInput from "../../components/CmsInput/CmsInput";
import CmsTextArea from "../../components/CmsTextArea/CmsTextArea";
import CmsFileUpload from "../../components/CmsFileUpload/CmsFileUpload";
import CmsSelectInput from "../../components/CmsSelectInput/CmsSelectInput";
import plusIcon from "../../images/plusIcon.svg";
import CmsInputButton from "../../components/CmsInputButton/CmsInputButton";
import CategoryModal from "../../components/CategoryModal/CategoryModal";
import { DefaultFormData } from "../../helpers/cmsFormData";
import validateNewProductData from "../../helpers/validateNewProductData"
import PageLoader from "../../components/PageLoader";
import { ToastContainer } from "react-toastify";
import baseUrl from "../../helpers/baseUrl";
import toaster from "../../helpers/toaster";

import "./contentManager.scss";

function ContentManager() {
  const [openModel, SetOpen] = useState(false);
  const [formData, SetFormData] = useState(DefaultFormData);
  const [categories, SetCategories] = useState([]);
  const [imgUrl, SetImgUrl] = useState('')
  const [errorBox, SetErrorBox] = useState([])
  const [loading, SetLoading] = useState(false)


  useEffect(() => {
    const fetchCategories = async () => {
      const url = baseUrl + "categories";
      const {
        data: { data },
      } = await axios.get(url);
      SetCategories(data);
    };
    fetchCategories().catch((error) => {
      console.log(error);
    });
  }, []);

  const toogleModal = (e) => {
    e.preventDefault();
    SetOpen(!openModel);
  };

  const handleChange = (e) => {
    const inputname = e.target.id;
    const inputValue = e.target.value;

    if (
      (inputname === "price" &&
        !/\d+$/g.test(inputValue) &&
        inputValue !== "") ||
      (inputname === "quantity" &&
        !/\d+$/g.test(inputValue) &&
        inputValue !== "")
    )
      return;

    const newValue = { ...formData, [inputname]: inputValue };
    SetFormData(newValue);
  };

  const addProduct = async () => {
    SetLoading(true)
    formData.image = imgUrl
    const errors = []
    const valid = validateNewProductData(formData, errors)
    if(!valid) {
      SetErrorBox(() => errors)
      SetLoading(false)
      return
    }

    try {
      
      const url = baseUrl + 'products'

      const {title, sku, price, image, category, quantity, description} = formData
      const payload = {
        "name": title,
        "categoryId": category,
        "item_description": description,
        "imgUrl": image,
        sku,
        price,
        quantity
      }
      const response = await axios.post(url, payload)
      SetLoading(false)
      SetFormData(DefaultFormData);
      SetImgUrl(null);
      toaster('success', response.data.message, 3000)
      
    } catch (error) {
      const message = error.response ? error.response.data.message : JSON.parse(error).message
      SetLoading(false)
      toaster('error', message, 3000)
    }
    

  };

  return (
    <div className="content_manager">
      <Header />
      <ToastContainer />
      <div className="content_fields_container">
        <div className="title_box">
          <span>
            <ArrowBack />
          </span>{" "}
          Add Product
        </div>

        <div className="form-control">
          <label htmlFor="title"> title </label>
          <CmsInput
            id="title"
            placeholder="Blue cotton T-shirt"
            value={formData.title}
            handler={handleChange}
          />
          {errorBox.indexOf("title") !== -1 && (
            <small className="error">Please add a product Title </small>
          )}
        </div>

        <div className="form-control">
          <label htmlFor="description"> description </label>
          <CmsTextArea
            id="description"
            value={formData.description}
            handler={handleChange}
          />
          {errorBox.indexOf("description") !== -1 && (
            <small className="error">Product cannot be Empty </small>
          )}
        </div>

        <div className="formfile-control">
          <CmsFileUpload
            id="image"
            label="Add file"
            value={imgUrl}
            setUrl={SetImgUrl}
          />
          {errorBox.indexOf("image") !== -1 && (
            <small className="error">Please add a Product Image </small>
          )}
        </div>

        <div className="form-control">
          <label htmlFor="price"> Price per item </label>
          <CmsInput
            id="price"
            placeholder="₦0.00"
            value={formData.price}
            handler={handleChange}
          />
          {errorBox.indexOf("price") !== -1 && (
            <small className="error">please review your price field</small>
          )}
        </div>

        <div className="form-control">
          <label htmlFor="sku"> SKU (Stock Keeping Unit) </label>
          <CmsInput
            id="sku"
            placeholder="sku-0001"
            value={formData.sku}
            handler={handleChange}
          />
          {errorBox.indexOf("sku") !== -1 && (
            <small className="error">please review your sku field</small>
          )}
        </div>

        <div className="form-control">
          <label htmlFor="quantity"> Quantity </label>
          <CmsInput
            id="quantity"
            placeholder="0"
            value={formData.quantity}
            handler={handleChange}
          />
          {errorBox.indexOf("quantity") !== -1 && (
            <small className="error">quantity cannot be Empty</small>
          )}
        </div>

        <div className="form-control">
          <label htmlFor="category"> Product Category</label>
          <div className="product_category">
            <div className="cat_cover">
              <CmsSelectInput
                id="category"
                placeholder="Select Category"
                value={formData.category}
                data={categories}
                handler={handleChange}
              />
              {errorBox.indexOf("category") !== -1 && (
                <small className="error">Please select a category </small>
              )}
            </div>
            <span className="cat_btn">
              <CmsInputButton
                label="New Category"
                icon={plusIcon}
                handler={toogleModal}
              />
            </span>
          </div>
        </div>

        <div className="form-control">
          <small>
            Please make sure all fields were filled correctly before proceeding.
          </small>
          <CmsInputButton label="Upload" handler={addProduct} />
        </div>
      </div>
      {loading && <PageLoader />}
      {openModel && <CategoryModal open={SetOpen} />}
    </div>
  );
}

export default ContentManager;
