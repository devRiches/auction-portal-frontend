import React,{useState} from 'react'
import AdminSidebar from "../../components/AdminSidebar";
import Header from "../../components/Header";
import OverviewBar from '../../components/OverviewBar';
import Piechart from '../../components/Piechart/Piechart';
import Barchart from '../../components/Barchart/Barchart';
import AdminTable from '../../components/AdminTable/AdminTable';
import './adminPages.scss'
import { useAdminDataContext } from '../../helpers/adminDataProvider'
import baseUrl from '../../helpers/baseUrl';
import axios from 'axios';
import toaster from '../../helpers/toaster';

function AdminDashboard() {
  const [searchEmail, setSearchEmail] = useState('')
  const {setAdminData, setLoading } = useAdminDataContext();
  
  const handleSearch = async(e) => {
    e.preventDefault();
    const url = baseUrl + "admin/user/transacttions";
    const payload = { email: searchEmail };

    try {
      setLoading(true);
      const {data} = await axios.post(url, payload);
      const filteredData = data.data.filter((data) => data.status === "paid");
      setAdminData(filteredData);
      setLoading(false)
      
    } catch (error) {
      setLoading(false);
      toaster("error", error.message, false);
    }

  }
    return (
      <div className="admin-dashboard-page">
        <Header />
        <div className="admin-dashboard-body">
          <AdminSidebar />
          <div className="admin-transaction-body__data">
            <OverviewBar />
            <div className="charts">
              <Piechart />
              <Barchart />
            </div>
            <div className="table__container">
              <div className="awaiting_pickup">Awaiting Pickup</div>
              <form className="admin-search-wrapper__search" onSubmit={handleSearch}>
                <i className="fas fa-search"></i>
                <input
                  type="text"
                  value={searchEmail}
                  onChange={(e) => setSearchEmail(e.target.value)}
                  placeholder="Search with email"
                />
              </form>
            </div>
            <AdminTable page="admin" />
          </div>
        </div>
      </div>
    );
}

export default AdminDashboard