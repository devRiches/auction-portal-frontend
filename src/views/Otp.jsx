import React, { useEffect, useState } from 'react'
import Header from '../components/Header'
import '../styles/otp.scss'
import OtpInput from 'react-otp-input';
import { useHistory} from 'react-router-dom'
import { ToastContainer } from 'react-toastify';
import toaster from '../helpers/toaster';
import CircularProgress from '@material-ui/core/CircularProgress';
import baseUrl from '../helpers/baseUrl';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';

function Otp() {
    const [otp, setOtp] = useState("");
    const userEmail = sessionStorage.getItem("email");
    const userNumber = sessionStorage.getItem("phoneNumber");
    const userName = sessionStorage.getItem("fullName");
    const [spinner, setSpinner] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [newEmail, setNewEmail] = useState('');
    const [open, setOpen] = useState(false);
    const [changeError, setChangeError] = useState('')

    useEffect(() => {
        
    }, [])
    const history = useHistory();
    // opens popup
    const handleClickOpen = () => {
        setOpen(true);
     };

     const renderInput = (inputProps, index) => (
        <input {...inputProps} key={index} />
      );
    //  closes popup
    const handleClose = () => {
        setOpen(false);
    };
    const handleChange = (otp) => {
        setOtp(otp);
    }
    // verifies otp
    const verifyOtp = () => {
            toaster('warn','Verifying otp..',1000);
            setSpinner(true)
            if(otp < 4){
                setSpinner(false)
                toaster('warn','Please enter complete otp',false);
                return
            }
            else{
                
                 // set payload
                 const payload ={
                         email: userEmail,
                         otp: otp
                 }
                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(payload)
        }
        // calling api using fetch
        fetch(`${baseUrl}users/verifyotp`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===200){
                console.log(data);
                 toaster('success', 'Sucessfull. Redirecting...', 1000);
                 setTimeout(() => {
                     sessionStorage.setItem("isLoggedIn", true);
                     history.push('/products')
                 }, 1000);
            }
            else{
                toaster('error', data.message, false);
                setErrorMessage(data.message)
                setSpinner(false)
            }
            
        })
        .catch(err=>{setErrorMessage(err)})

            }
          
    }
    // resends otp
    const resendOtp = () => {
          setSpinner(true);
          toaster('warn', "Resending Otp..", 1000)
          const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify({email: userEmail })
        }
        fetch(`${baseUrl}users/resendotp`, requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===200){
                console.log(data);
                toaster('success', data.message, 2000)
                setSpinner(false)
                setErrorMessage(data.message)
            }
            else{
                toaster('error', data.message, false);
                setErrorMessage(data.message)
                setSpinner(false)
            }
        })
    }
    //  updates user's email
    const changeEmail=()=>{
       
        sessionStorage.setItem('email', newEmail)
         toaster('warn', "Setting up with new email", 2000);
         setSpinner(true)
        const payload = {
            email: newEmail,
            phone: userNumber,
            fullname: userName
        }
        const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify(payload)
        }

              // calling api using fetch
        fetch(`${baseUrl}users/signup`,requestOptions)
        .then(response=>response.json())
        .then(data=>{
            if(data.status===201){
                toaster('success', "Email changed", 1000);
                sessionStorage.setItem('email', newEmail);
                setSpinner(false);
                setOpen(false);
            }
            else{
                toaster('error', data.message, 2000);
                setChangeError(data.message)
                setSpinner(false)
            }
            
        })
        .catch(err=>{setChangeError(err)})
    }
 
    return (
        <div className="otpContainer">
            <Header/>
            <ToastContainer/>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth>
                <DialogTitle id="form-dialog-title">Change Email</DialogTitle>
                     <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Email Address"
                            type="email"
                            fullWidth
                            onChange={(e)=>{setNewEmail(e.target.value)}}
                        />
                        </DialogContent>
                        <DialogActions>
                            {/* <Button onClick={handleClose} color="black">
                                Cancel
                            </Button> */}
                            <button onClick={handleClose}>
                                Cancel
                            </button>
                            <button onClick={changeEmail}>
                                {spinner && ( <CircularProgress size="15px" color="secondary"/>)}
                                 Change
                            </button>
                            {/* <Button onClick={changeEmail} color="black">
                                  {spinner && ( <CircularProgress size="20px" color="secondary"/>)}
                                Change
                            </Button> */}
                        </DialogActions>
                         <DialogContent>
                            {changeError}
                        </DialogContent>
            </Dialog>
            <div className="otp__container">
                <div className="otp__content">
                    <div className="otp__info">
                         <h1>Enter verification code</h1>
                          <div className="message">
                                 We have just sent a verification code to {userEmail}
                          </div>

                        <div className="otp">
                         <OtpInput
                            value={otp}
                            onChange={handleChange}
                            numInputs={4}
                            separator={<span></span>}
                            inputStyle = "otp__input"
                            renderInput={renderInput} // Pass the renderInput function as a p
                        />
                        <div className="resend__otp">
                            <span onClick={resendOtp}>Send the code again</span>  
                        </div>
                        <div className="resend__otp">
                            <span onClick={handleClickOpen}>Change email</span>  
                        </div>
                        <button onClick={verifyOtp}>
                            {spinner && ( <CircularProgress size="20px" />)}
                            VERIFY
                        </button>
                        <div className="error" style={{color:'red',textAlign:"center"}}>
                                {errorMessage}
                        </div>
                    </div>
                    </div>
                   
                   

                </div>
                <div className="image__contain"></div>
            </div>
        </div>
    )
}

export default Otp
