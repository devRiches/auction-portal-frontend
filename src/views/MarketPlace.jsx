import React from 'react'
import Products from '../components/Products/Products'
import '../styles/dashboard.scss'
import Sidebar from '../components/Sidebar';
import Header from '../components/Header';


function MarketPlace() {
  
    return (
        <div className="dashboard">
          <Header/>
          <div className="nav__side">
            <div className="sidebar">
               <Sidebar/>
            </div>
            <div className="products">
               <Products/>
            </div>
          </div>
         
        
        </div>
    )
}

export default MarketPlace;
