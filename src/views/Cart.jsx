import React from 'react'
import { useStateValue } from '../helpers/stateProvider'
import CurrencyFormat from 'react-currency-format';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { useHistory } from 'react-router-dom'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { IconButton } from '@material-ui/core';
import Header from '../components/Header';
import '../styles/cart.scss'
import OrderSummary from '../components/OrderSummary';
import MobileCart from '../components/MobileCart';

function Cart() {
    const [{ basket }, dispatch] = useStateValue();
    const history = useHistory();
 
    const returnToProduct = () => {
            history.push("/products")
    }
    const deleteItem = (id) => {
            dispatch({
                type:"REMOVE_FROM_BASKET",
                id:id
            })
    }
    return (
        <div className="cart">
               <Header/> 
            <div className="cart__container">
                 <div className="cart__header">Your Cart</div>
                 <div className="back__product">
                     <div className="return" onClick={returnToProduct}>
                        <KeyboardBackspaceIcon fontSize="small"/>
                        Continue Shopping
                     </div>
                     
                 </div>
                 <div className="cart__content">
                     <div className="all__orders">
                         <table>
                             <thead>
                                <tr>
                                 <th >Product</th>
                                 <th >Price</th>
                                 <th >Quantity</th>
                                 <th >Total</th>
                                 <th ></th>
                                </tr>
                             </thead>
                            <tbody>
                                   
                                  {basket.map(item=>
                                 (<tr key={item.id}>
                                     <td className="cart__image" colSpan="2"><img src={item.image} alt="" />{item.name}</td>
                                     <td className="cart__price" ><CurrencyFormat   value={item.price}
                                                                                    displayType={'text'}
                                                                                    thousandSeparator={true} 
                                                                                    prefix={'₦'} />
                                      </td>
                                     <td className="cart__quantity"><span>{item.quantityValue}</span></td>
                                     <td className="cart__total" > <CurrencyFormat   value={item.price * item.quantityValue}
                                                                                    displayType={'text'}
                                                                                    thousandSeparator={true} 
                                                                                    prefix={'₦'} />
                                     </td>
                                     <td className="cart__delete" > 
                                     <span onClick={()=>{deleteItem(item.id)}}>
                                            <IconButton>
                                                <DeleteForeverIcon/>
                                            </IconButton>
                                            
                                     </span>
                                     
                                     </td>
                                 </tr>)
                             )}
                            </tbody>
                         </table>
                          {basket.length === 0 && (<div className="empty__cart">Cart is Empty</div>)}
                     </div>
                     <div className="order__summary">
                         <OrderSummary/>
                     </div>
                 </div>
                 <div className="mobileCart">
                     <MobileCart/>
                 </div>
            </div>
              
            {/* <CurrencyFormat
                    renderText={(value) => (
                        <>
                            <h3 className="order__total">Order Total: {value}</h3>
                        </>
                    )}
                    decimalScale={2}
                    value={getBasketTotal(basket)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'₦'}

            /> */}
        </div>
    )
}

export default Cart
