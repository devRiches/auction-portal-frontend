export const initialState = {
    basket: JSON.parse(localStorage.getItem("basket")) || [],
    user: null,
    category: {
        name: 'ALL',
        categoryId: ''
    },
    search: "",
}
export const getBasketTotal = (basket) =>
    basket?.reduce((amount, item) => item.price * item.quantityValue + amount, 0)

const reducer = (state, action) => {
    switch (action.type) {
        case 'ADD_TO_BASKET':
            let extra = false;
            state.basket.map((item)=>{
                if (item.id === action.item.id) {
                    extra = true;
                    item.quantityValue += action.item.quantityValue;
                }
                return item
            })
            const basket = extra ? [...state.basket] : [...state.basket, action.item];
            localStorage.setItem('basket',JSON.stringify(basket))
            return {
                ...state,
                basket: basket
            }
        case 'REMOVE_FROM_BASKET':
            const index = state.basket.findIndex(
                (basketItem) => basketItem.id === action.id
            )
            let newBasket = [...state.basket];
            if (index >= 0) {
                newBasket.splice(index, 1)
            }
            else {
                console.warn(
                    `can't remove item with id ${action.id}`
                )
            }
            localStorage.setItem("basket", JSON.stringify(newBasket));
            return {
                ...state, basket: newBasket
            }
        case "SET_USER":
            return {
                ...state,
                user: action.user
            }
        case 'EMPTY_BASKET':
            localStorage.clear();
            return {
                ...state,
                basket: []
            }
        case 'PRODUCT_CATEGORY':
            return {
                ...state,
                category: action.category
            }
        case 'SEARCH_INPUT':
            return {
                ...state,
                search: action.search
            }
        default:
            return state
    }
}
export default reducer;