function validateNewProductData(data, errors){
    for(const field in data){
        
        if(data[field] === '')  errors.push(field)
    }

    if(errors.length) return false

    return true
}

export default validateNewProductData