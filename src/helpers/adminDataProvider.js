import React, { createContext, useContext, useState } from 'react'

    const row = [];

const adminDataContext = createContext()

const AdminDataContextProvider = ({children}) => {

  const [adminData, setAdminData] = useState(row);
  const [loading, setLoading] = useState(false)
  const [tab, setTab] = useState('all');
  const value = { adminData, setAdminData, loading, setLoading, tab, setTab };
  
    return (
      <adminDataContext.Provider value={value}>
        {children}
      </adminDataContext.Provider>
    );
}


const useAdminDataContext = () => {
    const context = useContext(adminDataContext);
    return context
}




export { AdminDataContextProvider, useAdminDataContext };