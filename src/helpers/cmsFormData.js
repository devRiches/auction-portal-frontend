export const DefaultFormData = {
  title: "",
  description: "",
  image: "",
  price: "",
  sku: "",
  quantity: "",
  category: "",
};